import { Service } from "typedi";
import { ObjectType, Field, Resolver, Arg, Query, Int } from "type-graphql"
import { TodoService } from "../services";


@ObjectType()
export class Todo {
	@Field(_ => Int)
	id: number

	@Field(_ => String)
	title: string

	@Field(_ => Boolean)
	completed: boolean
}

@Service()
@Resolver(Todo)
export class TodoResolver {

	constructor(private readonly todoService : TodoService) {}

	@Query(_ => [Todo])
	async todos(
		@Arg("offset", _ => Int, { nullable: true }) offset: number = 0,
		@Arg("limit", _ => Int, { nullable: true }) limit: number = Number.MAX_SAFE_INTEGER
	) {
		let data = await this.todoService.getTodos(offset, limit)
		return data.slice(offset, limit)
	}
	@Query(_ => Todo)
	async todo(@Arg("id", _ => Int) id: number) {
		let data = await this.todoService.getTodo(id)
		return data
	}
}