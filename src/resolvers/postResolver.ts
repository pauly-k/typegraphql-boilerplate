import { Service } from "typedi";
import { ObjectType, Field, Resolver, Arg, Query, Int, FieldResolver, Root } from "type-graphql"
import { PostService } from "../services/PostService";
import { User } from "./UserResolver";
import { UserService } from "../services";

@ObjectType()
export class Post {
	@Field(_ => Int)
	id: number

	@Field(_ => Int)
	userId: number

	@Field(_ => String)
	title: string

	@Field(_ => String)
	body: string

	@Field(_ => User)
	user: User

}

@Service()
@Resolver(Post)
export class PostResolver {

	constructor(
		private readonly postService : PostService,
		private readonly userService : UserService
	) {}

	@Query(_ => [Post])
	async posts(
		@Arg("offset", _ => Int, { nullable: true }) offset?: number,
		@Arg("limit", _ => Int, { nullable: true }) limit?: number
	) {
		offset = offset || 0
		limit = limit || Number.MAX_SAFE_INTEGER
		let data = await this.postService.getPosts(offset, limit)
		return data.slice(offset, limit)
	}
	
	@Query(_ => Post)
	async post(@Arg("id", _ => Int) id: number) {
		let data = await this.postService.getPost(id)
		return data
	}

	@FieldResolver()
	async user(@Root() post: Post) {
		return this.userService.getUser(post.userId)
	} 

}