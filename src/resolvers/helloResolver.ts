import { Resolver, Query, Arg } from "type-graphql";


@Resolver() 
export class HelloResolver {
	@Query(() => String)
	async hello(@Arg('name') name: string) {
		return `hello ${name}`
	}
}