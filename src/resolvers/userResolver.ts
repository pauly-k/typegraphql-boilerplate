import { ObjectType, Field, Resolver, Query, Arg, Int } from "type-graphql";
import { UserService } from "../services";


@ObjectType()
export class User {
	@Field()
	id: number

	@Field()
	username: string

	@Field()
	email: string
}

@Resolver(_ => User)
export class UserResovler {
	constructor(private service: UserService) {}

	@Query(_ => User)
	async user(@Arg("id") id: number) {
		return this.service.getUser(id)
	}

	@Query(_ => [User])
	async users(
		@Arg("offset", _ => Int, { nullable: true }) offset: number = 0,
		@Arg("limit", _ => Int, { nullable: true }) limit: number = Number.MAX_SAFE_INTEGER
	) {
		return this.service.getUsers(offset, limit)
	}
}