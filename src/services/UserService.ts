import fetch from 'node-fetch'
import { Service } from 'typedi'

const baseUrl ='https://jsonplaceholder.typicode.com/users'

@Service()
export class UserService {
	async getUser(id: number) {
		return fetch(`${baseUrl}/${id}`).then(r => r.json())
	}
	async getUsers(offset: number, limit: number) {
		const data = await fetch(baseUrl).then(r => r.json())
		return data.slice(offset, offset + limit)
	}
}