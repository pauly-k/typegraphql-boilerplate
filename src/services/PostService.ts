import fetch from 'node-fetch'
import { Service } from 'typedi'

@Service()
export class PostService {
  async getPosts(offset: number = 0, limit: number = Number.MIN_SAFE_INTEGER) {
    let data = await fetch(
      'https://jsonplaceholder.typicode.com/posts'
    ).then((resp: any) => resp.json())
    return data.slice(offset, limit)
  }
  async getPost(id: number) {
    let data = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}`
    ).then((resp: any) => resp.json())
    return data
  }
}
