import fetch from 'node-fetch'
import { Service } from 'typedi'

@Service()
export class TodoService {
  async getTodos(offset: number = 0, limit: number = Number.MIN_SAFE_INTEGER) {
    let data = await fetch(
      'https://jsonplaceholder.typicode.com/todos'
    ).then((resp: any) => resp.json())
    return data.slice(offset, limit)
  }
  async getTodo(id: number) {
    let data = await fetch(
      `https://jsonplaceholder.typicode.com/todos/${id}`
    ).then((resp: any) => resp.json())
    return data
  }
}
