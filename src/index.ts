import 'reflect-metadata'
import { ApolloServer } from 'apollo-server-express'
import express from 'express'
import { buildSchema } from 'type-graphql'
import { HelloResolver, TodoResolver, PostResolver } from './resolvers'
import { Container } from 'typedi'
import { UserResovler } from './resolvers/UserResolver'

const main = async () => {
	
	const schema = await buildSchema({  
		resolvers: [HelloResolver, TodoResolver, PostResolver, UserResovler],
		container: Container
	});

	const server = new ApolloServer({ schema })

	const app = express()
	server.applyMiddleware( { app })

	const port = process.env.PORT || 4400
	app.listen({ port}, () => 
	 console.log(`listening on http://localhost:${port}${server.graphqlPath}`)
	)
}

main()
