# Setup

* [benawad/type-graphql-series](https://github.com/benawad/type-graphql-series)

```bash
npm i apollo-server-express express graphql reflect-metadata type-graphql
npm i -D @types/express  @types/node ts-node ts-node-dev typescript
touch tsconfig.json
```
